---
layout: page
subheadline: Releases
title:  "Metamorph - Lamashtu"
teaser: "In Sumerian and Assyrian mythologies, Lamashtu is considered a malevolent demon representing childbed fever."

breadcrumb: false
categories:
    - releases
tags:
    - news
    - releases
    - metamorph
header:
   image_fullwidth  : "black.png"
author: matthes
---

![Metamorph - Lamashtu Cover](/images/metamorph_lamashtu.jpg "Metamorph - Lamashtu")

## Facts
* Label:	Deathinteresse Records – DIR 001
* Format:	Album
* Country:	Germany
* Release Date:	2009-05-09
* Genre:	Progessive Death Metal

## Tracklist
1. You Are Not Forgiven
2. Gost Of Wind
4. Lamashtu Pt. 1: A Thousand Lifeless Shards
5. Lamashtu Pt. 2 Dance Of The Praying Mantis
6. Lamashtu Pt. 3 Tabula Rasa
7. New World Depression
