---
layout: page
title:  "Upcoming release: new EITER album"
teaser: the new album of Münster grindcore slashers <strong>EITER</strong> will be released under DIR.
date:   2023-03-31 18:00:00 +0100
categories: news
author: matthes
header:
   image_fullwidth  : "black.png"
---
After a long period of silence, we are excited to announce that the upcoming album of Münster grindcore slashers <strong>EITER</strong> will
be released under Deathinteresse Records. Mixed and mastered at the [Kohlekeller Studio](https://www.kohlekeller.de/) the release is scheduled for 30th of June 2023
and will feature 13 tracks with a total length of 19:20 minutes.

Watch out for any single releases until then!