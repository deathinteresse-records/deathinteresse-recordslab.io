---
layout: page
title: "About DIR"
subheadline: "Death Metal for people with a disinterest."
permalink: "/info/"
header:
   image_fullwidth  : "black.png"
date: "2023-05-05"
---
Deathinteresse Records was founded in 2009 as an underground death metal label in Münster (Westfalia), Germany.

Christoph Wagner and Matthes Rieke, both also running local community website muensterland-metal.de, decided
to form an informative label for the upcoming release of Metamorph's debut album "Lamashtu". The name was quickly
settled as both had started a concert series in Münster called "Deathinteressiert?!" in late 2008 / early 2009.

![Deathinteressiert?!](/images/deathinteresse_series.jpg "Deathinteressiert?!")

After a longer break due to life, Deathinteresse Records is back in 2023 with releasing music from Münster's fine
underground metal scene.