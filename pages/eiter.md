---
layout              : page
show_meta           : false
title               : "EITER"
subheadline         : "Powergrind from Münster, Westfalia"
teaser              : "2 minutes is long for a song...!"
header:
   image_fullwidth  : "eiter-1.jpg"
permalink           : "/artists/eiter/"
date: "2023-05-05"
---

![EITER - Logo](/images/eiter_logo.png "EITER")


<div class="artist-bio">
<p>
Münster needs more grindcore - and the world anyway. EITER is celebrated by five guys who smash their contempt in the face of humanity. King Chaos has thrown barely tolerable nihilism, filthy humor and nasty noise into one pot. The result is EITER.
</p>
<p>
In 2012 they were still sitting at the bar, but in 2013 they did find their way into the cramped, rancid rehearsal space. In 2014, they started blow-drying perms down people's necks at some concerts. The performance is extremely sweaty, aggressive and forces you to freak out.
</p>
<p>
It goes without saying that this power must also work on plastic for the people. So in the summer of 2014 they locked themselves in Dave Sustain's (LONG DISTANCE CALLING) bunker in the "SOUND RANGER" studio and pounded eleven bitterly evil symphonies into the recording equipment. Jojo Brunn of GO-RECORDING did the mastering for the festering soup. Sebastian Jerke (AHAB, ZODIAC, LONG DISTANCE CALLING, etc.) conjured up an adequate logo and their point of view appropriately visualized. Concept and design has been cobbled together from the meltdown of the band's brains. In 2015, this first EP was released on their own. This was followed by various live activities for example with Nashgul, Choked By Own Vomits, Morbid Mosh Attack, Postmortem, Rotten Casket, Bloodspot and many more. In the process, the sweaty crowd was talked into buying the appropriate merchandise.
</p>
<p>
Then in 2019, on a whim, a "Christmas album" was created for the reflective days, which was released in digital form on Bandcamp.
</p>
<p>
In 2020, work then got underway on the new EP entitled "GEWALT". Since there was corona everywhere, rehearsals were impossible. Therefore, we decided that the songwriting will be divided among the guitarists. In the process, 13 rock-strong tracks came out. The drums were again recorded by Jojo Brunn at GO-RECORDING in 2021. Guitars and vocals were recorded at Matthes. The bass was recorded by Tom himself at home. Re-amping, Mix & mastering were done by Kohlekeller, who cobbled together a proper asocial sound.
</p>
</div>


## The Band

![EITER - Promo Picture](/images/eiter_picture.jpg "EITER")

* Throat: Striego
* Axe: Flo
* Saw: Matthes
* Hatchet: Tom
* Kettle: Leo

## Social

* <a href="https://eiter666.bandcamp.com">bandcamp</a>
* <a href="https://www.facebook.com/eiter.grind">facebook</a>
* <a href="https://www.instagram.com/eiter.666">instagram</a>
* <a href="https://www.youtube.com/channel/UCzm7q5tTf9wnQE9EVfR67Lg">youtube</a>
