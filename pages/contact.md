---
layout              : page
title               : "Contact"
meta_title          : "Contact us"
subheadline         : "Contact DIR"
teaser              : "Get in touch with us? Use the email."
header:
   image_fullwidth  : "black.png"
permalink           : "/contact/"
date: "2023-04-30"
---
E-Mail: <span class="goto" >durroon -ät- mailbox.org</span><br>