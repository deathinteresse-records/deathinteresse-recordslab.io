---
layout              : page
show_meta           : false
title               : "DIR Artists"
subheadline         : "Portfolios"
header:
   image_fullwidth  : "eiter-1.jpg"
permalink           : "/artists/"
date: "2023-04-30"
---



<p><a href="/artists/eiter" style="border: 0"><img src="/images/eiter_logo.png"></a></p>


<p><a href="/artists/metamorph" style="border: 0"><img src="/images/metamorph_logo.png"></a></p>