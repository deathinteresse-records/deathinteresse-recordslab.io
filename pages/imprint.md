---
layout              : page
title               : "Imprint / Impressum"
subheadline         : "Deathinteresse Records"

header:
   image_fullwidth  : "black.png"
permalink           : "/imprint/"
date: "2023-05-05"
---

<div class="col-12" >
<div class="cms-element-text">

<b>Legal website operator identification</b>
<p>
Deathinteresse Records<br>
Matthes Rieke<br>
Wiegandweg 17<br>
48167 Münster<br>
Germany<br>
E-Mail: <span class="goto" >durroon -ät- mailbox.org</span><br>

</p>
<br>
<p>
<strong>Alternative dispute resolution:</strong><br>
The European Commission provides a platform for the out-of-court resolution of disputes (ODR platform), which can be viewed under <a href="https://ec.europa.eu/consumers/odr" target="_blank">https://ec.europa.eu/odr</a>.
</p>

</div>
</div>