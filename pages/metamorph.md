---
layout              : page
show_meta           : false
title               : "Metamorph"
subheadline         : "Progressive Death Metal"
teaser              : "10 minutes is short for a song...!"
header:
   image_fullwidth  : "metamorph-1.jpg"
permalink           : "/artists/metamorph/"
date: "2023-04-30"
---

![Metamorph - Logo](/images/metamorph_logo.png "Metamorph")

<div class="artist-bio">
<p>
Metamorph was founded January ´07 by a bunch of musicians, each coming from unsatisfying
random projects. Previous changes of the line-up, genre and the name could quite rapidly be
forgotten because of this new constellation. In spite of differing personal tastes in music, a
consensus was determined concerning composition, structures of the songs and expectations
of the own music also. Finally the first songs were worked out, which one could pigeonhole in
the broad area of melodic Death Metal.
</p>
<p>
During 2007 Metamorph performed several gigs with some local, averagely known bands and
the decision was made to simultaneously record a demo-CD “Do-it-Yourself-Style”. It took
almost a year to do so, but in January ´08 Metamorph released their untitled Demo containing
10 songs resulting in 50 minutes of Melodic Death Metal.
</p>
<p>
In 2008, beside seizing every opportunity to perform, new songs filling a new album were
created, being a distinct development compared to the Demo in degree of hardness, com-
plexity, awesomeness...
</p>
<p>
Metamorph began to record these new songs in autumn, again D.i.Y, and finished their second
Album in January 2009. This album is entitled “Lamashtu” and was released on 8. May 2009. It
contains 6 songs yielding 50 minutes of - best described with - non-conform Death Metal.
you are now up to date, go and spread the news!
</p>
</div>


## The Band

![Metamorph - Promo Picture](/images/metamorph_picture.jpg "Metamorph")

* Vocals: Anja Zgoda
* Lead/rhythm guitars, backing vocals: Matthes Rieke
* Lead/rhythm guitars, backing vocals: Jens Hoppe
* Bass: Philipp Gröning
* Drums: Christoph Wagner

## Social

<!-- * <a href="https://eiter666.bandcamp.com">bandcamp</a> -->
* <a href="https://facebook.com/people/Metamorph/100066879056992/">facebook</a>
<!-- * <a href="https://www.instagram.com/eiter.666">instagram</a> -->
* <a href="https://www.youtube.com/@MetamorphMetal">youtube</a>

